package com.msci.wealthbench.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.msci.wealthbench.model.Employee;
import com.msci.wealthbench.model.Response;
import com.msci.wealthbench.serviceimpl.EmployeeServiceImpl;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

@RestController
@RequestMapping(value = "/emp")
public class EmployeeController {

	@Autowired
	EmployeeServiceImpl employeeServiceImpl;

	@PostMapping("/create")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@ApiOperation(value = "Create New employee", authorizations = { @Authorization(value = "Token") })
	public ResponseEntity<Response> createEmployee(@RequestBody Employee emp) {
		return new ResponseEntity<>(new Response(HttpStatus.OK.value(), "success", HttpStatus.OK.getReasonPhrase(),
				employeeServiceImpl.createEmployee(emp)), HttpStatus.CREATED);
	}

	@DeleteMapping("/delete")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@ApiOperation(value = "delete existing employee", authorizations = { @Authorization(value = "Token") })
	public void deleteEmployee(@RequestBody Employee emp) {
		employeeServiceImpl.deleteEmployee(emp);
	}

	@PostMapping("/delete/{id}")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@ApiOperation(value = "delete employee by id", authorizations = { @Authorization(value = "Token") })
	public void deleteEmployeeByID(@PathVariable("id") Long id) {
		employeeServiceImpl.deleteEmployeeByID(id);
	}

	@GetMapping("/getAll")
	@PreAuthorize("hasAnyRole('ADMIN','USER')")
	@ApiOperation(value = "get All employee", authorizations = { @Authorization(value = "Token") })
	public ResponseEntity<Response> getAllEmployee() {
		return new ResponseEntity<>(new Response(HttpStatus.OK.value(), "success", HttpStatus.OK.getReasonPhrase(),
				employeeServiceImpl.getAllEmployees()), HttpStatus.OK);
	}

	@GetMapping("/get/{id}")
	@PreAuthorize("hasAnyRole('ADMIN','USER')")
	@ApiOperation(value = "get employee by id", authorizations = { @Authorization(value = "Token") })
	public ResponseEntity<Response> getEmployeeByID(@PathVariable("id") Long id) {
		return new ResponseEntity<>(new Response(HttpStatus.OK.value(), "success", HttpStatus.OK.getReasonPhrase(),
				employeeServiceImpl.getEmployeeById(id)), HttpStatus.OK);
	}

	@GetMapping("/getByName/{name}")
	@PreAuthorize("hasAnyRole('ADMIN','USER')")
	@ApiOperation(value = "get employee by name", authorizations = { @Authorization(value = "Token") })
	public ResponseEntity<Response> getEmployeeByName(@PathVariable("name") String name) {
		return new ResponseEntity<>(new Response(HttpStatus.OK.value(), "success", HttpStatus.OK.getReasonPhrase(),
				employeeServiceImpl.getByname(name)), HttpStatus.OK);
	}

	@GetMapping("/getCount")
	@PreAuthorize("hasAnyRole('ADMIN','USER')")
	@ApiOperation(value = "get total employee count", authorizations = { @Authorization(value = "Token") })
	public long getEmployeeCount() {
		return employeeServiceImpl.countEmployees();
	}

	@PutMapping("/save")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@ApiOperation(value = "update existing employee data", authorizations = { @Authorization(value = "Token") })
	public ResponseEntity<Response> updateEmployee(@RequestBody Employee emp) {
		return new ResponseEntity<>(new Response(HttpStatus.OK.value(), "success", HttpStatus.OK.getReasonPhrase(),
				employeeServiceImpl.editEmployee(emp)), HttpStatus.OK);
	}

}