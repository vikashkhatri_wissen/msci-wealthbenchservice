package com.msci.wealthbench.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.msci.wealthbench.model.Response;
import com.msci.wealthbench.model.UserRegistration;
import com.msci.wealthbench.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

@RestController
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	UserService userService;

	@PostMapping("/createUser")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@ApiOperation(value = "Create New role", authorizations = { @Authorization(value = "Token") })
	public ResponseEntity<Response> insertUser(@RequestBody UserRegistration user) {

		return new ResponseEntity<>(new Response(HttpStatus.OK.value(), "success", HttpStatus.OK.getReasonPhrase(),
				userService.createUser(user)), HttpStatus.CREATED);
	}

}
