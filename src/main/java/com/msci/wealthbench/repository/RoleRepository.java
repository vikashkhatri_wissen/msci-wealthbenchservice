package com.msci.wealthbench.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.msci.wealthbench.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

	// List<Optional<Role>> findAllByRole_Id(List<Integer> roleIds);

	@Query(value = "SELECT u FROM Role u WHERE u.role IN :roles")
	List<Role> findRoles(@Param("roles") Collection<String> roles);
}
