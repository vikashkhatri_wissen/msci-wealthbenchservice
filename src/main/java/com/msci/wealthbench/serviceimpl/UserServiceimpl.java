package com.msci.wealthbench.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.msci.wealthbench.enums.Roles;
import com.msci.wealthbench.model.UserRegistration;
import com.msci.wealthbench.model.Users;
import com.msci.wealthbench.repository.RoleRepository;
import com.msci.wealthbench.repository.UsersRepository;
import com.msci.wealthbench.service.UserService;

@Service
public class UserServiceimpl implements UserService {

	@Autowired
	UsersRepository usertRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Override
	public Users createUser(UserRegistration regUser) {
		Users user = new Users();
		user.setActive(regUser.getActive());
		user.setEmail(regUser.getEmail());
		user.setName(regUser.getName());
		user.setLastName(regUser.getLastName());
		String password = regUser.getPassword();
		user.setPassword(encoder.encode(password));
		String[] roles = regUser.getRole();
		List<String> roleList = new ArrayList<>();
		for (String role : roles) {
			Roles enumRole = Roles.valueOf(role);
			switch (enumRole) {
			case ADMIN:
				roleList.add("ADMIN");
				break;

			case USER:
				roleList.add("USER");
				break;
			}
		}
		user.setRoles(roleRepository.findRoles(roleList));
		return usertRepository.save(user);
	}

}
