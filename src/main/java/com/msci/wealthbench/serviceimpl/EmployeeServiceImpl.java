package com.msci.wealthbench.serviceimpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.msci.wealthbench.model.Employee;
import com.msci.wealthbench.repository.EmployeeRepository;
import com.msci.wealthbench.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public long countEmployees() {
		return employeeRepository.count();
	}

	@Override
	public Employee createEmployee(Employee employee) {
		return employeeRepository.save(employee);
	}

	@Override
	public void deleteEmployee(Employee employee) {
		employeeRepository.delete(employee);
	}

	@Override
	public void deleteEmployeeByID(Long id) {
		employeeRepository.deleteById(id);
	}

	@Override
	public Employee editEmployee(Employee employee) {
		return employeeRepository.save(employee);
	}

	@Override
	public Iterable<Employee> getAllEmployees() {
		return employeeRepository.findAll();
	}

	@Override
	public Employee getByname(String name) {

		return employeeRepository.getByname(name);
	}

	@Override
	public Optional<Employee> getEmployeeById(Long id) {
		return employeeRepository.findById(id);
	}

}
