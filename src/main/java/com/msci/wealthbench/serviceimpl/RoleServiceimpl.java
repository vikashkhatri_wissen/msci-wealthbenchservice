package com.msci.wealthbench.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.msci.wealthbench.model.Role;
import com.msci.wealthbench.repository.RoleRepository;
import com.msci.wealthbench.service.RoleService;

@Service
public class RoleServiceimpl implements RoleService {

	@Autowired
	RoleRepository roleRepository;

	@Override
	public List<Role> findRoles(List<String> roles) {

		return roleRepository.findRoles(roles);
	}

}
