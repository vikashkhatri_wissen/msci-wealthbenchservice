package com.msci.wealthbench.model;

import javax.validation.constraints.NotBlank;

public class JwtUser {

	@NotBlank
	private String userName;

	@NotBlank
	private String password;

	public String getPassword() {
		return password;
	}

	public String getUserName() {
		return userName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
