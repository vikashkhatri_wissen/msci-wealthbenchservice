package com.msci.wealthbench.service;

import java.util.List;

import com.msci.wealthbench.model.Role;

public interface RoleService {

	List<Role> findRoles(List<String> roles);

}
