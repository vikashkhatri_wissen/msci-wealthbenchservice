package com.msci.wealthbench;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean
	public Docket api() {

		List<SecurityScheme> schemeList = new ArrayList<>();
		/* schemeList.add(new BasicAuth("BasicAuth")); */
		ApiKey apiKey = new ApiKey("Token", "Authorization", "header");
		schemeList.add(apiKey);

		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.msci.wealthbench.controller")).paths(PathSelectors.any())
				.build().apiInfo(apiInfo()).securitySchemes(schemeList)
				.securityContexts(Arrays.asList(securityContext()));
	}

	private ApiInfo apiInfo() {
		return new ApiInfo("DashBoard rest API", "REST API for dashboard", "0.0.1-SNAPSHOT", "Terms of service",
				new Contact("vikash kumar", "www.wissentechnolody.com", "vikash.khatri@wissen.com"), "Public Licence",
				"API license URL", Collections.emptyList());
	}

	@Bean
	public SecurityConfiguration security() {
		return SecurityConfigurationBuilder.builder().useBasicAuthenticationWithAccessCodeGrant(true).build();
	}

	private SecurityContext securityContext() {
		return SecurityContext.builder().forPaths(PathSelectors.any()).build();
	}

}
