package com.msci.wealthbench.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.msci.wealthbench.model.JwtUser;
import com.msci.wealthbench.model.Response;
import com.msci.wealthbench.security.JwtGenerator;
import com.msci.wealthbench.service.UserService;

@RestController
@RequestMapping("/token")
public class TokenController {

	@Autowired
	private JwtGenerator jwtGenerator;

	@Autowired
	UserService userService;

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	PasswordEncoder encoder;

	@PostMapping("/generate")
	public ResponseEntity<Response> generate(@RequestBody JwtUser jwtUser) {

		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(jwtUser.getUserName(), jwtUser.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		return new ResponseEntity<>(new Response(HttpStatus.OK.value(), "success", HttpStatus.OK.getReasonPhrase(),
				jwtGenerator.generateToken(authentication)), HttpStatus.OK);

	}

	/*
	 * @PostMapping("/createUser") public ResponseEntity<Response>
	 * insertUser(@RequestBody UserRegistration user) { return new
	 * ResponseEntity<>(new Response(HttpStatus.OK.value(), "success",
	 * HttpStatus.OK.getReasonPhrase(), userService.createUser(user)),
	 * HttpStatus.CREATED); }
	 */

}
