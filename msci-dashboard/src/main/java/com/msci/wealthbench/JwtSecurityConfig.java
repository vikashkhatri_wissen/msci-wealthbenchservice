package com.msci.wealthbench;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.msci.wealthbench.security.JwtAuthenticationEntryPoint;
import com.msci.wealthbench.security.JwtAuthenticationTokenFilter;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
@Configuration
public class JwtSecurityConfig extends WebSecurityConfigurerAdapter {

	/*
	 * @Autowired private JwtAuthenticationProvider authenticationProvider;
	 *
	 * @Autowired private JwtAuthenticationEntryPoint entryPoint;
	 *
	 * @Bean public AuthenticationManager authenticationManager() { return new
	 * ProviderManager(Collections.singletonList(authenticationProvider)); }
	 *
	 * @Bean public JwtAuthenticationTokenFilter authenticationTokenFilter() {
	 * JwtAuthenticationTokenFilter filter = new JwtAuthenticationTokenFilter();
	 * filter.setAuthenticationManager(authenticationManager());
	 * filter.setAuthenticationSuccessHandler(new JwtSuccessHandler()); return
	 * filter; }
	 *
	 *
	 * @Override protected void configure(HttpSecurity http) throws Exception {
	 *
	 * http.csrf().disable()
	 * .authorizeRequests().antMatchers("/emp/**").authenticated() .and()
	 * .exceptionHandling().authenticationEntryPoint(entryPoint) .and()
	 * .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	 *
	 * http.addFilterBefore(authenticationTokenFilter(),
	 * UsernamePasswordAuthenticationFilter.class); http.headers().cacheControl();
	 *
	 * }
	 */

	@Autowired
	UserDetailsService customUserDetailsService;

	@Autowired
	private JwtAuthenticationEntryPoint unauthorizedHandler;

	@Override
	@Bean(BeanIds.AUTHENTICATION_MANAGER)
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public JwtAuthenticationTokenFilter authenticationTokenFilter() {
		return new JwtAuthenticationTokenFilter();
	}

	/*
	 * @Bean public JwtAuthenticationTokenFilter authenticationTokenFilter() throws
	 * Exception { JwtAuthenticationTokenFilter filter = new
	 * JwtAuthenticationTokenFilter();
	 * filter.setAuthenticationManager(authenticationManagerBean());
	 * filter.setAuthenticationSuccessHandler(new JwtSuccessHandler()); return
	 * filter; }
	 */

	@Override
	public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		authenticationManagerBuilder.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder());
		// authenticationManagerBuilder.parentAuthenticationManager(authenticationManagerBean()).userDetailsService(customUserDetailsService);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests().antMatchers("/emp/**").authenticated().and().exceptionHandling()
				.authenticationEntryPoint(unauthorizedHandler).and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		http.addFilterBefore(authenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class);

	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

}
