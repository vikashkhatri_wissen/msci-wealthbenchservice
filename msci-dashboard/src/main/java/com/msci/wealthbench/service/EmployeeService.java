package com.msci.wealthbench.service;

import java.util.Optional;

import com.msci.wealthbench.model.Employee;

public interface EmployeeService {

	Employee createEmployee(Employee Employee);
	Optional<Employee> getEmployeeById(Long id);
	Employee editEmployee(Employee Employee);
	void deleteEmployee(Employee Employee);
	void deleteEmployeeByID(Long id);
	Iterable<Employee> getAllEmployees();
	long countEmployees();
	Employee getByname(String name);
	
}