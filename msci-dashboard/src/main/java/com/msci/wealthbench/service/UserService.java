package com.msci.wealthbench.service;

import com.msci.wealthbench.model.UserRegistration;
import com.msci.wealthbench.model.Users;

public interface UserService {

	Users createUser(UserRegistration user);

}
