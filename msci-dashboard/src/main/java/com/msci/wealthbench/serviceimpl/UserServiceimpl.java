package com.msci.wealthbench.serviceimpl;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.msci.wealthbench.model.Role;
import com.msci.wealthbench.model.UserRegistration;
import com.msci.wealthbench.model.Users;
import com.msci.wealthbench.repository.UsersRepository;
import com.msci.wealthbench.service.UserService;

@Service
public class UserServiceimpl implements UserService {

	@Autowired
	UsersRepository repo;

	@Autowired
	PasswordEncoder encoder;

	@Override
	public Users createUser(UserRegistration regUser) {
		Users user = new Users();
		user.setActive(regUser.getActive());
		user.setEmail(regUser.getEmail());
		user.setName(regUser.getName());
		user.setLastName(regUser.getLastName());
		String password = regUser.getPassword();
		user.setPassword(encoder.encode(password));
		Set<Role> set = Arrays.stream(regUser.getRole()).map(a -> new Role(a)).collect(Collectors.toSet());
		user.setRoles(set);
		return repo.save(user);
	}

}
