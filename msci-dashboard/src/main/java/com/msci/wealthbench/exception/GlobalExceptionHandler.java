package com.msci.wealthbench.exception;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.msci.wealthbench.model.Response;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(BadCredentialsException.class)
	public ResponseEntity<Response> handleBadCredentialsException(HttpServletRequest request,
			HttpServletResponse response, BadCredentialsException ex) {
		return new ResponseEntity<>(new Response(0, "error", ex.getMessage(), null), HttpStatus.UNAUTHORIZED);

	}

	@ExceptionHandler(IOException.class)
	public ResponseEntity<Response> handleIOException(IOException ex) {
		return new ResponseEntity<>(new Response(0, "error", ex.getMessage(), null), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(SQLException.class)
	public ResponseEntity<Response> handleSQLException(HttpServletRequest request, HttpServletResponse response,
			SQLException ex) {
		return new ResponseEntity<>(new Response(0, "error", ex.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);

	}
}
