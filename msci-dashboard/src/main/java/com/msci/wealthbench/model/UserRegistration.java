package com.msci.wealthbench.model;

public class UserRegistration {

	private String email;
	private String password;
	private String name;
	private String lastName;
	private int active;
	private String[] role;

	public int getActive() {
		return active;
	}

	public String getEmail() {
		return email;
	}

	public String getLastName() {
		return lastName;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	public String[] getRole() {
		return role;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setRole(String[] role) {
		this.role = role;
	}

}
